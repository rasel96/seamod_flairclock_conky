# SeaMod_FlairClock_Conky


1. Install conky by putting `sudo apt-get install conky-all` in the terminal.
2. Extract the `conky_seamod_flairClock.zip` to your `Home` directory.
2. Put `conkyStart.sh` to your `Home` directory.
3. Make it executable by putting `chmod +x conkyStart.sh` in the terminal.
4. Run it by putting `./conkyStart.sh` in the terminal.
5. To add it to startup, open **Startup Applications** from **Dash Menu**.
6. Click `add`.
7. Give it a `Name`.
8. Browse `conkyStart.sh` for *Command*.
9. Add *comment* if you want.
10. Reboot or Log out and Log in again.

All credit goes to the original developer of **SeaMod Conky** and **Flair Weather Clock**.
But these versions are not compatible with the latest Conky versions as conky now follows the LUA Syntax.
I just changed necessary syntax and directories to make it work.
Couldn't make the Weather working, so i put some of my favourite quotes. Feel free to change it.

Don't forget to change the wallpaper!
